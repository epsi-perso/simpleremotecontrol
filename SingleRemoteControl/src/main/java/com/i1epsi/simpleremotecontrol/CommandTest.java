/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.simpleremotecontrol;

import com.i1epsi.simpleremotecontrol.vendor.GarageDoor;
import com.i1epsi.simpleremotecontrol.vendor.Lights;

/**
 *
 * @author TVall
 */
public class CommandTest {
    public static void main(String[] args) {
        Lights lights = new Lights();
        GarageDoor garageDoor = new GarageDoor();
        //lights.lightsOn();
        AlexaCommand alexaCommand = new LightsOnCmd(lights);
        AlexaCommand alexaCommandDoor = new GarageDoorUpCmd(garageDoor);
        
        SimpleRemoteControl simpleRemoteControl = new SimpleRemoteControl(alexaCommandDoor);
        simpleRemoteControl.pressButton();
    }
}
