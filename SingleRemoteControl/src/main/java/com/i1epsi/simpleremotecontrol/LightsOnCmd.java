/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.simpleremotecontrol;

import com.i1epsi.simpleremotecontrol.vendor.Lights;

/**
 *
 * @author TVall
 */
public class LightsOnCmd implements AlexaCommand{
    private Lights lights;
    public LightsOnCmd(Lights lights){
        this.lights = lights;
    }
    
    @Override
    public void execute(){
        lights.lightsOn();
    }
}
