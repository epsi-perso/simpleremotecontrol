/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.i1epsi.simpleremotecontrol;

import com.i1epsi.simpleremotecontrol.vendor.Lights;
import com.i1epsi.simpleremotecontrol.vendor.WindowBlinds;

/**
 *
 * @author TVall
 */
public class SimpleRemoteControl {

    private AlexaCommand alexaCommand;

    public SimpleRemoteControl(AlexaCommand alexaCommand) {
        this.alexaCommand = alexaCommand;
    }
    
    
    public AlexaCommand getAlexaCommand() {
        return alexaCommand;
    }

    public void setAlexaCommand(AlexaCommand alexaCommand) {
        this.alexaCommand = alexaCommand;
    }
    
    public void pressButton(){
        alexaCommand.execute();
    }
}
