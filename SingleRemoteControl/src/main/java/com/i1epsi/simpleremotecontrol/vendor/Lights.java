/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.simpleremotecontrol.vendor;

/**
 *
 * @author TVall
 */
public class Lights {
    public void lightsOn(){
        System.out.println("lights on");
    }
    
    public void lightsOff(){
        System.out.println("lights off");
    }
}
