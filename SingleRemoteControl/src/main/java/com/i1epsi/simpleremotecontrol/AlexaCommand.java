/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.simpleremotecontrol;

/**
 *
 * @author TVall
 */
public interface AlexaCommand {
    void execute();
}
